import React ,{Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'

class ItemList extends Component {
 


    render(){  

        var listStyle={
            'overflowX':'scroll',
            'position':'fixed',
            'bottom':'1%',
            'display':'flex',
            'border':'1px solid black',
            'height':'120px',
            'width':'100%',
            'margin':'1%',
            'marginLeft':'50%',
            'transform':'translateX(-50%)',
            'padding':'3%'
        }
        var listItemStyle={
            'position':'relative',
            'border':'1px solid green',
            'width':'230px',
            'height':'70px',
            'margin':'1%',
            'padding':'6.5%',
            'fontSize':'11px',
            'fontWeight':'bolder'
        }

        var addButtonStyle = {
            'position':'absolute',
            'border':'1px solid green',
            'background':'green',
            'color':'white',
            'marginTop':'15%',
            'width':'70%',
            'bottom':'15%',
            'left':'50%',
            'transform':'translateX(-50%)'
        }

        return(
        <StoreConsumer>
            {( { state, action } ) => (
                <div style={listStyle}>
                    {
                    Object.entries(state.inListItems).map((item, i) => {
                        return <div style={listItemStyle} 
                                    key={i} name={item[1].name}>
                                 
                                    <div>
                                        <div>{`${item[1].name}`}</div>
                                        <div>{`${item[1].price}`}</div>
                                        {/* <div>{`${item[1].description}`}</div> */}

                                    </div>
                                    <div>
                                        <button style={addButtonStyle} onClick={  () => action.inCart(item[1])}>add</button>
                                    </div>
                                </div>;})
                    }
                </div>
            )}
        </StoreConsumer>
         
        )
    }

}



export default ItemList