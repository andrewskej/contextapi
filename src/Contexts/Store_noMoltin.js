import React, { Component, createContext } from 'react'; 
import { gateway as MoltinGateway } from '@moltin/sdk'; //죽어도 안되더니 npm i 하고 비벼대니 된다..
const clientId = 'yrVXGA67JHPhqHbkkMgBwp1ZBg6kLk6NKbWfhmNbMJ';
const referenceId = '1885751072186696559';
const Moltin = MoltinGateway({ client_id: clientId });

const { Provider, Consumer: StoreConsumer } = createContext();
class StoreProvider extends Component {
	//선택삭제, 이미지...

	state = {
		inCartItems: [],
		inListItems: [
			{ name: '싼거', price: 50, qty: 1, isChecked: true },
			{ name: '최저가', price: 80, qty: 1, isChecked: true },
			{ name: '중저가', price: 100, qty: 1, isChecked: true },
			{ name: '고오급', price: 1000, qty: 1, isChecked: true },
			{ name: '최고급', price: 9000, qty: 1, isChecked: true },
			{ name: '최고가', price: 75000, qty: 1, isChecked: true },
			{ name: '진짜비싼거', price: 92560, qty: 1, isChecked: true },
			{ name: '쓸데없는거', price: 25000, qty: 1, isChecked: true },
			{ name: '갖고싶은거', price: 350000, qty: 1, isChecked: true },
			{ name: '별거아닌거', price: 5000, qty: 1, isChecked: true }
		],
		finalPrice: 0,
		allChecked: true,
		checkedItems: 0
	};

	componentDidMount() {
		// Moltin SDK
		Moltin.Cart(referenceId)
			.Items()
			.then(({ data }) => {
				console.log(data);
				data.forEach(item => console.log(item.name))
			});
	}

	action = {
		inCart: itemsToCart => {
			if (this.state.inCartItems.filter(el => el.name === itemsToCart.name).length > 0) {
				const itemToIncrease = this.state.inCartItems.filter(el => el.name === itemsToCart.name);
				this.setState({
					qty: (itemToIncrease[0].qty += 1)
				});
			} else {
				this.setState({
					inCartItems: [...this.state.inCartItems, itemsToCart]
				});
			}
		},

		onIncrease: currentItem => {
			const itemToIncrease = this.state.inCartItems.filter(el => el.name === currentItem.name);
			this.setState({
				qty: (itemToIncrease[0].qty += 1)
			});
		},

		onDecrease: currentItem => {
			const itemToDecrease = this.state.inCartItems.filter(el => el.name === currentItem.name);
			this.setState({
				qty: (itemToDecrease[0].qty -= 1)
			});

			//1. 여기 버그가 있는듯 하다...언체크 되어있는 아이템을 - 나 x버튼으로 빼면 다음 index 체크드아이템에 체크가 풀려...
			if (currentItem.qty < 1) {
				this.setState({
					inCartItems: this.state.inCartItems.filter(item => item.name !== currentItem.name),
					qty: (itemToDecrease[0].qty += 1)
				});
			}

			//1. 이렇게 해결? 뭔가 찝찝한데
			const stateChecked = this.state.inCartItems.filter(item => item.isChecked === true);
			const realChecked = document.getElementsByName('oneCheck');
			console.log('state:', stateChecked);
			console.log('real:', realChecked);
			for (var i = 0; i < stateChecked.length; i++) {
				realChecked[i].checked = stateChecked[i].isChecked;
			}
		},

		//2. 여기 버그가 있는듯 하다...언체크 되어있는 아이템을 - 나 x버튼으로 빼면 다음 index 체크드아이템에 체크가 풀려...
		outCart: itemsFromCart => {
			itemsFromCart.qty = 1;
			this.setState({
				inCartItems: this.state.inCartItems.filter(item => item.name !== itemsFromCart.name),
				allChecked: false
			});

			//2. 이렇게 해결? 좀 찝찝... 코드중복도 있고
			const stateChecked = this.state.inCartItems.filter(item => item.isChecked === true);
			const realChecked = document.getElementsByName('oneCheck');
			console.log('state:', stateChecked);
			console.log('real:', realChecked);
			for (var i = 0; i < stateChecked.length; i++) {
				realChecked[i].checked = stateChecked[i].isChecked;
			}
		},

		handleCheck: selectedItem => {
			selectedItem.isChecked = !selectedItem.isChecked;
			const itemsToCheck = this.state.inCartItems.filter(item => item.name === selectedItem);
			this.setState({
				isChecked: itemsToCheck.isChecked
			});
		},

		allCheckControl: () => {
			var allChk = document.querySelector('input');
			var oneChks = document.getElementsByName('oneCheck');

			if (allChk.checked === true) {
				oneChks.forEach(item => {
					item.checked = true;
				});
				this.state.inCartItems.forEach(item => (item.isChecked = true));
			} else {
				oneChks.forEach(item => {
					item.checked = false;
				});
				this.state.inCartItems.forEach(item => (item.isChecked = false));
			}

			const checkedPrice = this.state.inCartItems.filter(item => item.isChecked !== false);
			const fPrice = checkedPrice.map(item => item.price * item.qty);

			var fP = 0;
			for (let i = 0; i < fPrice.length; i++) {
				fP += fPrice[i];
			}

			this.setState({
				finalPrice: fP
			});
		}
	};

	//3. 여기서 덜어내서 각 메소드로 보낼수 있을 코드는 발라내서 분배 해보자 여기때문에 로직이 꼬이는것 같다
	componentDidUpdate(prevProps, prevState) {
		const prices = this.state.inCartItems
			.filter(item => item.isChecked === true)
			.map(item => item.price * item.qty);

		let fPrice = 0;
		for (let i = 0; i < prices.length; i++) {
			fPrice += prices[i];
		}

		if (prevState.finalPrice !== fPrice) {
			this.setState({
				finalPrice: fPrice
			});

			const checkedItemsList = this.state.inCartItems.filter(item => item.isChecked === true);

			this.setState({
				checkedItems: checkedItemsList.length
			});

			if (this.state.inCartItems.length !== 0) {
				if (checkedItemsList.length === 0) {
					document.querySelector('input').checked = false;
					this.setState({
						allChecked: false
					});
				}
				if (checkedItemsList.length === this.state.inCartItems.length) {
					document.querySelector('input').checked = true;
					this.setState({
						allChecked: true
					});
				}
			}
		}
	}

	render() {
		const { state, action } = this;
		const stateAndAction = { state, action };

		return <Provider value={stateAndAction}>{this.props.children}</Provider>;
	}
}

export { StoreProvider, StoreConsumer };
