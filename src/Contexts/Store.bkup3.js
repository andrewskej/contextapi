import React, {Component, createContext} from 'react'

//Provider 는 미리 이름 정해주면 안되는 건가?
//안되겠지 바로 밑에서 이름 지어서 클래스를 만드니까..?
const {Provider, Consumer:StoreConsumer} = createContext()

class StoreProvider extends Component{

    //얘가 기본 값이 된다 
    //필요한 정보를 ItemList.js 와 CartList.js 에서 각각 뽑아가서 보여주는..
    state = {
        inCartItems: [],
        inListItems: [
            {name:'test1', price:500, qty:1},
            {name:'test2', price:600, qty:1},
            {name:'test3', price:700, qty:1},
        ]
        ,quantity:1
    }

    //action 으로 메소드를 만든다(다른데서 갖다 쓸거야)
    action = {
        inCart:(itemsToCart) => {
            console.log('sent to Store from List:',itemsToCart)
            // console.log('중복방지 작업 필요')
            this.setState({
               inCartItems: [...this.state.inCartItems, itemsToCart]
            })
            console.log('currently in Cart:',this.state.inCartItems)
        },
        //클릭한 아이템의 갯수를 1씩 올리려면?
        onIncrease:(currentItem)=>{
            //모든 아이템 값이 올라간다. 
            //선택한 특정 아이템의 qty만 올려야지
            // const {qty} = currentItem.qty
            const {qty} = this.state.inCartItems  //이거 솔직히 잘 이해가 안간다

            //state 내의 inCartItems 에서, 내가 고른것과 같은 이름의 아이템을 찝어? filter? 
            // const chosen = this.state.inCartItems.filter((el) => el.name == currentItem.name)
            // console.log('...:',chosen[0].qty)
            // const qty = chosen[0].qty
            // const {thisItem} = chosen[0]
            // console.log(chosen[0])
            // qty = qty.qty
            //this.state.inCartItems 는 현재 아이템목록
            //여기 뒤에 어떻게 뽑아야 되지?
            // const {qty} = this;
            this.setState({
                //현재 카트 내 아이템 목록을 map 해서 모든 qty에 1씩 더하고 있잖아..
                // qty: this.state.inCartItems 
                qty: this.state.inCartItems.map( (chosen) => chosen.qty += 1   )
               
                //어쩌면 액션이 필요없고 CartList.js 에서 해결해야하는거 아닐까? 잘 안됐음..
            })
            // console.log('current:',currentItem)

            // this.state.inCartItems.map( (el) => { if(el.name == currentItem.name) return currentItem.qty += 1}  )

            // console.log('onInc:',currentItem)
        },
        onDecrease:()=>{

        },
        outCart:(itemsFromCart) =>{
            console.log(itemsFromCart)
            // console.log('remove from cart..중복방지 작업 해야해')
            this.setState({
                inCartItems: this.state.inCartItems.filter( (item) => item.name !== itemsFromCart.name  )
            })
        }
        
    }

    render(){
        //기본값과 내가 만든 메소드를 변수로 담아서
        //옆에 애들이 쓸수있는 Provider의 value 로 담아서 내보내
        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            //Provider 로 감싸서 타 클래스에서 사용가능하게 열어준다?
            <Provider value={stateAndAction}>
                {/* 얘의 props(App.js? 맞나? 아니고 CartList.js 인듯? )
                의 children은 누구야? 얘를 끌어다 쓰는 애의 속성을 보여준다는 건가?*/}
                {this.props.children}
            </Provider>
        )
    }

}

//내보낼땐 기본으로 불러온 객체 Provider, Consumer 가 아니고
//내가 만든 인스턴스? 를 내보내서 사용한다.
export {StoreProvider, StoreConsumer}