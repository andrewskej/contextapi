import React, {Component, createContext} from 'react'

//Provider 는 미리 이름 정해주면 안되는 건가?
//안되겠지 바로 밑에서 이름 지어서 클래스를 만드니까..?
const {Provider, Consumer:StoreConsumer} = createContext()

class StoreProvider extends Component{

    //얘가 기본 값이 된다 - CartList.js 에서 뽑아가서 보여주는..
    //Store 가 중간에서 왔다갔다 해도 되는데..왜 객체는!?!?안돼???
    state = {
        inCartItems: [],
        // inListItems: '1'
    }

    //action 으로 메소드를 만든다(다른데서 갖다 쓸거야)
    action = {
        setValue:(inCartItems) => {
            this.setState({
               inCartItems: [...this.state.inCartItems,' , ',inCartItems]
            })
        }
    }

    render(){
        //기본값과 내가 만든 메소드를 변수로 담아서
        //옆에 애들이 쓸수있는 Provider의 value 로 담아서 내보내
        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            //Provider 로 감싸서 타 클래스에서 사용가능하게 열어준다?
            <Provider value={stateAndAction}>
                {/* 얘의 props(App.js? 맞나? 아니고 CartList.js 인듯? )
                의 children은 누구야? 얘를 끌어다 쓰는 애의 속성을 보여준다는 건가?*/}
                {this.props.children}
            </Provider>
        )
    }

}

//내보낼땐 기본으로 불러온 객체 Provider, Consumer 가 아니고
//내가 만든 인스턴스? 를 내보내서 사용한다.
export {StoreProvider, StoreConsumer}