import React from 'react'
import {StoreConsumer} from '../Contexts/Store'

//ItemList 에서 보낸걸 여기서 받으려고 한다
//얘는 클래스가 아니고 그냥 함수. 차이는?
//클래스일땐 뭐가 가능하고 아닐땐 뭐가 불가능하지?
const CartList = () => {

//얘는 state가 필요 없네? 왜지?
//받아서 보여주기만 해서?

return(
    //Consumer로 랩했다는 것은 Provider의 데이터를 
    //가져다가 쓰겠다는 뜻이다. 맞지? 
    <StoreConsumer>
        {
            //여기서 cart 라는 것은 CartProvider 에서 가져온 값을
            //왜 cart 라고 이름이 바뀌지? cart 라고 내보낸 적 없는데?
            //더 알기쉽게 provider 하고 consumer 이름을 store 로 바꿔보았다
            (store) => (
                //CartProvider 에서 cart 로 받아온 값
                //의 state value 즉 기본값 && 입력받은 값을 표시해 준다
                //Store에서 state내의 변수명 바꾸면 여기도 맞춰야 된다

                <div>
                   카트: {store.state.inCartItems}
                </div>
            )
                
        }
    </StoreConsumer>
)



}

export default CartList