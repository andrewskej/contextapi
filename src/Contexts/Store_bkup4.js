import React, {Component, createContext} from 'react'

const {Provider, Consumer:StoreConsumer} = createContext()

class StoreProvider extends Component{

    //얘를 두 리스트 컴포넌트 사이에 기준점으로 잡는다  
    //필요한 정보를 (좌)ItemList.js 와 (우)CartList.js 에서 각각 뽑아가서 보여주는 구조
    state = {
        inCartItems: [],
        inListItems: [
            {name:'싸구려', price:50, qty:1},
            {name:'중저가', price:600, qty:1},
            {name:'고오급', price:2700, qty:1},
        ],
        //얘를 계산하는 함수는 어디로? 스토어에? 여기 아닌거 아닌가? 일단 여기 하단에서 계산은 했다
        finalPrice: 0
    }

    action = {
        inCart:(itemsToCart) => {
            //고른 아이템이 이미 배열 안에 있는 거라면
            if(this.state.inCartItems.filter((el) => el.name === itemsToCart.name).length > 0){
                // this.onIncrease(itemsToCart)  //이렇게 자기 자신의 함수 재소환은 안되나부네..
                //됐다!!!!!!!!!!!!!!!!!!중복방지
                const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === itemsToCart.name)
                this.setState({
                    //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                    qty: itemToIncrease[0].qty +=1   
                })
            }else{
                this.setState({
                    inCartItems: [...this.state.inCartItems, itemsToCart]
                    //비중복 넣을때는 초기값 1로 해주고 싶은데..전에 뺀적 있으면 그걸로 지정되어 버림
                })
            }
            //왜 처음 하나 들어가면 [] 부터 나오지? 
            console.log('currently in Cart:',this.state.inCartItems)
        },

        //이거다!!!!!!!!!!!!!!!!!!!분석 꼭 해보기
        onIncrease:(currentItem) => {
            console.log(currentItem)
            //내가 고른 아이템 찾기
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                qty: itemToIncrease[0].qty +=1,
            })
        },

        onDecrease:(currentItem)=>{
            console.log(currentItem)
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                qty:itemToIncrease[0].qty -=1  
            })
            //0되면 outCart 효과가 나게끔
            //되었다!!!!!!!!!!
            if(currentItem.qty < 1){
                this.setState({
                    inCartItems: this.state.inCartItems.filter( (item) => item.name !== currentItem.name  ),
                    //이게 진짜 값 초기화라고 할수 있나? 억지로 0일때 1 더해서 눈속임인데...
                    qty:itemToIncrease[0].qty +=1
                })
            }
        },

        outCart:(itemsFromCart) =>{
            console.log(itemsFromCart)
            this.setState({
                inCartItems: this.state.inCartItems.filter( (item) => item.name !== itemsFromCart.name  ),
            })
            //뺐다가 다시 애드하면 수량이 그대로 유지됨...1로 초기화 해야 하지 않을까?
        }
        
    }


    componentDidUpdate(prevProps,prevState){
        console.log('xxx:',prevState.finalPrice)
        
        // console.log('store - price update?')
        //여기서 가격조정 해야될것 같은데?
        //이렇게 하면 새 배열에 각 아이템 가격*갯수 한것들이 들어가고...얘를 다 변수 한개로 뭉쳐야지
        const prices = this.state.inCartItems.map( (item) => item.price * item.qty  )
        console.log(prices)

        var fPrice = 0; //let 쓰면 스코프가 안들어가는데 걍 var 말고는 방법 없나 let 쓰고싶은데..
        for(let i = 0; i<prices.length; i++){
            fPrice += prices[i];
        }
        // console.log(fPrice) //이게 최종 가격인데!!!!  뽑았는데!!!!!
        //얘를 어디서 가져다 꽂을수 있지??뭐만 하면 에러여
        // return fPrice;
        //됐다!!!!!!!!!!!!!!!!!!!!! 기존값과 새 값을 비교해서 변화가 있을때만 setState..그냥 쓰면 무한루프에러
        if(prevState.finalPrice !== fPrice){
            console.log('time to change')
            this.setState({
                finalPrice:fPrice               
            })
        }else{
            console.log('no change')
        }



    }


    
    render(){


        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            <Provider value={stateAndAction}>
                {this.props.children}
            </Provider>
        )
    }

}

export {StoreProvider, StoreConsumer}