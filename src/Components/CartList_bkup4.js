import React, {Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'
class CartList extends Component {

    allCheckControl = () => {  
        var allChk = document.getElementsByName('allCheck')
        var oneChks = document.getElementsByName('oneCheck')
        console.log(allChk[0].checked)        
        //일단 클릭연결은 됐는데 이걸 state로 가격변동에 연결해야돼...
        if(allChk[0].checked === true){
            oneChks.forEach((chk)=>{(chk.checked = true)})
        }else{
            oneChks.forEach((chk)=>{(chk.checked = false)})
        }
    }

    render(){
        
        var cartStyle = {
            'position':'fixed',
            'top':'2%',            
            'border':'1px solid gold',
            'width':'70%',
            'height':'62%',
            'overFlow':'scroll-Y',
            'margin':'1%',
            'marginLeft':'50%',
            'transform':'translateX(-50%)',
            'padding':'3%',
            'textAlign':'center'
        }

        var cartItemStyle = {
            'border':'1px solid orangered',
            'width':'70%',
            'margin':'2%',
            'marginLeft':'50%',
            'transform':'translateX(-50%)',
            'padding':'3%',
            'fontWeight':'bolder'
        }

        var buttonStyle1 = {
            'border':'1px solid black',
            'background':'white',
            'marginTop':'15%',
            'color':'black',
            'fontSize':'18px'
        }

        var buttonStyle2 = {
            'border':'1px solid black',
            'background':'red',
            'color':'white',
            'fontSize':'18px'
        }

        var checkBoxStyle = {
            'position':'fixed',
            'border':'1px solid black',
            'width':'18px',
            'height':'18px',
            'left':'8%'            
        }


    return(
            <StoreConsumer>
                {( { state, action } ) => (
                    <div style={cartStyle}> motherfucking CART 
                        {/* <input type="checkBox" name="allCheck" style={checkBoxStyle}  onChange={(e)=>action.handleAllCheck(e)}/> */}
                        <input type="checkBox" name="allCheck" style={checkBoxStyle}  onClick={this.allCheckControl}/>

                            {
                            Object.entries(state.inCartItems).map((item, i) => {
                                
                                return <div style={cartItemStyle} key={i} name={item[1].name}>
                                           <input style={checkBoxStyle} name="oneCheck" type="checkBox" defaultChecked onChange={()=> action.handleCheck(item[1])}/>
                                        
                                           <div>
                                                <div>{`${item[1].name}`}</div>
                                                <div>{`$: ${item[1].price}`}</div>
                                                <div>수량:{item[1].qty}</div>
                                            </div>

                                            <div>
                                                <button style={buttonStyle1} onClick={ () => action.onDecrease(item[1]) }>-</button>
                                                <button style={buttonStyle2} onClick={ () => action.outCart(item[1])}>remove</button>
                                                <button style={buttonStyle1} onClick={ () => action.onIncrease(item[1]) }>+</button>
                                            </div>
                                        </div>
                            })

                        }
                        </div>
                    )}
            </StoreConsumer>
        )
    }



}

export default CartList