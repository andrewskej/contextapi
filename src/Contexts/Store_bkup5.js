import React, {Component, createContext} from 'react'

const {Provider, Consumer:StoreConsumer} = createContext()

class StoreProvider extends Component{

    //얘를 두 리스트 컴포넌트 사이에 기준점으로 잡는다  
    //필요한 정보를 (좌)ItemList.js 와 (우)CartList.js 에서 각각 뽑아가서 보여주는 구조
    state = {
        inCartItems: [
        ],
        inListItems: [
            {name:'싸구려', price:50, qty:1, isChecked:true},
            {name:'중저가', price:600, qty:1, isChecked:true},
            {name:'고오급', price:2700, qty:1, isChecked:true},
        ],
        //얘를 계산하는 함수는 어디로? 스토어에? 여기 아닌거 아닌가? 일단 여기 하단에서 계산은 했다
        finalPrice: 0,
        allChecked: true
    }

    action = {
        inCart:(itemsToCart) => {

            //고른 아이템이 이미 배열 안에 있는 거라면
            if(this.state.inCartItems.filter((el) => el.name === itemsToCart.name).length > 0){
                // this.onIncrease(itemsToCart)  //이렇게 자기 자신의 함수 재소환은 안되나부네..
                //됐다!!!!!!!!!!!!!!!!!!중복방지
                const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === itemsToCart.name)
                this.setState({
                    //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                    qty: itemToIncrease[0].qty +=1,
                })
            }else{
                this.setState({
                    inCartItems: [...this.state.inCartItems, itemsToCart]
                    //비중복 넣을때는 초기값 1로 해주고 싶은데..전에 뺀적 있으면 그걸로 지정되어 버림
                })
            }


        },

        //이거다!!!!!!!!!!!!!!!!!!!분석 꼭 해보기
        onIncrease:(currentItem) => {
            console.log(currentItem)
            //내가 고른 아이템 찾기
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                qty: itemToIncrease[0].qty +=1,
            })
        },

        onDecrease:(currentItem)=>{
            console.log(currentItem)
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                qty:itemToIncrease[0].qty -=1  
            })
            //0되면 outCart 효과가 나게끔
            //되었다!!!!!!!!!!
            if(currentItem.qty < 1){
                this.setState({
                    inCartItems: this.state.inCartItems.filter( (item) => item.name !== currentItem.name  ),
                    //이게 진짜 값 초기화라고 할수 있나? 억지로 0일때 1 더해서 눈속임인데...
                    qty:itemToIncrease[0].qty +=1
                })
            }
        },

        outCart:(itemsFromCart) => {
            console.log(itemsFromCart)
            this.setState({
                inCartItems: this.state.inCartItems.filter( (item) => item.name !== itemsFromCart.name  ),
//              allChecked:true  //not sure about this...
            })

            //뺐다가 다시 애드하면 수량이 그대로 유지됨...1로 초기화 해야 하지 않을까?
        },
        handleCheck:(selectedItem) => {
            //isChecked 를 반전 시켜야...
            // console.log(selectedItem)
            // console.log(selectedItem.isChecked)
            //클릭에 따른 상태 반전 이제 먹는것 가튼데?
            selectedItem.isChecked = !selectedItem.isChecked
            // console.log(selectedItem.isChecked)

            const itemsToCheck = this.state.inCartItems.filter( (item)=> item.name === selectedItem)
            this.setState({isChecked:itemsToCheck.isChecked})

            // console.log('체크가 빠지면 금액도 빼야 된다..흠') <- 뺐다
        },

        //다음은 전체 체크박스
        //다 켜져있으면 얘도 켜져야 하고 하나라도 끄면 꺼지게 할까?
        //클릭하면 다 켜지고, 재클릭시 다 꺼지고 - 헬로네이처거랑 다른 로직일듯 ..난 이게 더 직관적이라 생각하니까 ㅎㅎ
        //allChecked 로 가자

        handleAllCheck:(e) => {  //allCheck 를 클릭했을때
            const ifClicked = e.target.checked
            if(ifClicked === true){
                console.log('다 체크 시켜야')
                //개별 체크박스를 어떻게 가져오지? 여기가 아니고 CartList 에서 해야되나..
            }else{
                //모든 체크박스 꺼야 되는데..
                console.log('전부 체크 해제 시켜야')
            }

            this.setState({
                allChecked: !this.state.allChecked
            })
            // console.log(this.state.allChecked)
            //true 면 모든 아이템 체크 시켜야하고 반대면 빼야한다..?
        }
    }


    componentDidUpdate(prevProps,prevState){
        //prevProps 는 안쓰는것 같아도 인자에서 빼버리면 무한 루프가 터진다..일단 킵
        // 이건 모든 카트 내 아이템의 가격을 합산하는 것이다(체크여부 무시)
        // const prices = this.state.inCartItems.map( (item) => item.price * item.qty  )

        //isClicked 가 true 인 것만 필터 해서 맵을 해줘! 그러면 되지 않을까?
        //돼!!!!!!!!fuck yeah man
        const prices = this.state.inCartItems.filter(  (item) => item.isChecked === true ).map( (item) => item.price * item.qty  )

        this.state.inCartItems.filter((item) => console.log(item))

        var fPrice = 0; //let 쓰면 스코프가 안들어가는데 걍 var 말고는 방법 없나 let 쓰고싶은데..
        for(let i = 0; i<prices.length; i++){
            fPrice += prices[i];
        }
    

        if(prevState.finalPrice !== fPrice){
            this.setState({
                finalPrice:fPrice               
            })
        }

       // 전체선택 여기 마자? 근데 로직이 좀 이상...
       var allChk = document.getElementsByName('allCheck')
       if(this.state.inCartItems.length>0){
           allChk[0].checked = true; //아이템이 하나라도 있으면 올체크 켜지고
       }else{
           allChk[0].checked = false; //하나도 없으면 꺼지고?
           //하나라도 빠지면 또 꺼야 되는거 아냐?
           //여기랑 CartList.js 의 로직이 둘 다 각각 필요한거 맞나?
           //통합 방법은 없나? 
       }
    }

    
    render(){

        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            <Provider value={stateAndAction}>
                {this.props.children}
            </Provider>
        )
    }

}

export {StoreProvider, StoreConsumer}