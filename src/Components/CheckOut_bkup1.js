import React, {Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'
import Coupons from './Coupons'
class CheckOut extends Component{



    render(){

        var checkOutStyle={
            'position':'absolute',
            'bottom':'20%',
            'transform':'translateX(25%)',
            'border':'solid 2px black',
            'width':'60%',
            'height':'12%',
            'padding':'3%',
            'margin':'3%',
            'textAlign':'center',
            'fontWeight':'bold'
        }

        var finalStyle={
            'color':'orangeRed',
            'position':'absolute',
            'bottom':'35%',
            'left':'25%'
        }
         
        var bonusStyle={
            'position':'absolute',
            'bottom':'10%',
            'left':'25%'
        }

        var couponStyle ={
            'position':'absolute',
            'width':'60%',
            'left':'25%'
        }

        return(
            <StoreConsumer>
                
                {(store) => (

                    //store 의 inCartItems 내에 있는 각 아이템의 price * qty 를 더한 값;
                    //cart 가 업데이트 될때마다 숫자가 변해야 하니까...흠
                    //reduce 이럴때 쓰는 건가? 누적값 ? 
                    //.toLocaleString(); 이런거 먹일려면??

                    <div style={checkOutStyle}>
                        {/* coupon 구현중... */}
                        <select style={couponStyle}>
                            <Coupons></Coupons>
                        </select>
                    
                        <div className="finalPrice">
                          <span style={finalStyle}>최종 금액: $ {store.state.finalPrice}</span>
                        </div>
                        <div style={bonusStyle}> 적립금: $ {store.state.finalPrice * 0.15} </div>
                    </div>
                
                )}   
            </StoreConsumer>
        )

    }


}

export default CheckOut