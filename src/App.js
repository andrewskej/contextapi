import React, { Component } from 'react';
import {StoreProvider} from './Contexts/Store'
import CartList from './Components/CartList'
import ItemList from './Components/ItemList'
import CheckOut from './Components/CheckOut'
class App extends Component {
  render() {

   var titleStyle={
     'color':'red',
     'fontWeight':'bolder',
     'marginLeft':'38%',
     'marginBottom':'3%'
   }

    return (
      <div>
        <span style={titleStyle}> 카트!!!!! </span>
        <StoreProvider>
            <CartList/>
            <CheckOut/>
            <ItemList/>
        </StoreProvider>
      </div>

    );
  }
}

export default App;
