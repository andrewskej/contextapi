import React, { Component, createContext } from 'react'; 
import { gateway as MoltinGateway } from '@moltin/sdk'; 
// const clientId = 'yrVXGA67JHPhqHbkkMgBwp1ZBg6kLk6NKbWfhmNbMJ';
// const referenceId = '1885751072186696559';
const clientId = '0AfLeJQlZI4w9cXeRHQKJsWEz7oIZOC3AlIk3lZqLg'; //my own
const referenceId = '1910740243082903939';//my own
const Moltin = MoltinGateway({ client_id: clientId });

const { Provider, Consumer: StoreConsumer } = createContext();
class StoreProvider extends Component {

	state = {
		inCartItems: [],
		inListItems: [
			{id:1, name:'abc',price:100,unit_price:{amount:100},image:{href:""}, quantity:1, isChecked:true},
			{id:2, name:'zzz',price:300,unit_price:{amount:300},image:{href:""}, quantity:1, isChecked:true},
			{id:3, name:'xyz',price:500,unit_price:{amount:500},image:{href:""}, quantity:1, isChecked:true},
		],
		finalPrice: 0,
		allChecked: true,
		checkedItems: 0
	};

	componentDidMount() {
		// Moltin SDK
		// console.log(Moltin)
		// Moltin.Cart(referenceId)
		// 	.Items()
		// 	.then(({ data }) => {
		// 		data.forEach(item => 
		// 			item.isChecked = true)
		// 		data.forEach(item=> 
		// 			item.price = item.unit_price.amount)

		// 		this.setState({
		// 			inListItems: data
		// 		}) 
		// 	});
	}


	action = {
		inCart: itemsToCart => {
			if (this.state.inCartItems.filter(el => el.name === itemsToCart.name).length > 0) {
				const itemToIncrease = this.state.inCartItems.filter(el => el.name === itemsToCart.name);
				this.setState({
					quantity: (itemToIncrease[0].quantity += 1)
				});
			} else {
				this.setState({
					inCartItems: [...this.state.inCartItems, itemsToCart]
				});
			}
		},

		onIncrease: currentItem => {
			const itemToIncrease = this.state.inCartItems.filter(el => el.name === currentItem.name);
			this.setState({
				quantity: (itemToIncrease[0].quantity += 1)
			});
		},

		onDecrease: currentItem => {
			const itemToDecrease = this.state.inCartItems.filter(el => el.name === currentItem.name);
			this.setState({
				quantity: (itemToDecrease[0].quantity -= 1)
			});

			if (currentItem.quantity < 1) {
				this.setState({
					inCartItems: this.state.inCartItems.filter(item => item.name !== currentItem.name),
					quantity: (itemToDecrease[0].quantity += 1)
				});
			}

			const stateChecked = this.state.inCartItems.filter(item => item.isChecked === true);
			const realChecked = document.getElementsByName('oneCheck');
			for (var i = 0; i < stateChecked.length; i++) {
				realChecked[i].checked = stateChecked[i].isChecked;
			}
		},

		outCart: itemsFromCart => {
			itemsFromCart.quantity = 1;
			this.setState({
				inCartItems: this.state.inCartItems.filter(item => item.name !== itemsFromCart.name),
				allChecked: false
			});

			const stateChecked = this.state.inCartItems.filter(item => item.isChecked === true);
			const realChecked = document.getElementsByName('oneCheck');
			for (var i = 0; i < stateChecked.length; i++) {
				realChecked[i].checked = stateChecked[i].isChecked;
			}
		},

		handleCheck: selectedItem => {
			selectedItem.isChecked = !selectedItem.isChecked;
			const itemsToCheck = this.state.inCartItems.filter(item => item.name === selectedItem);
			this.setState({
				isChecked: itemsToCheck.isChecked
			});
		},

		allCheckControl: () => {
			var allChk = document.querySelector('input');
			var oneChks = document.getElementsByName('oneCheck');

			if (allChk.checked === true) {
				oneChks.forEach(item => {
					item.checked = true;
				});
				this.state.inCartItems.forEach(item => (item.isChecked = true));
			} else {
				oneChks.forEach(item => {
					item.checked = false;
				});
				this.state.inCartItems.forEach(item => (item.isChecked = false));
			}

			const checkedPrice = this.state.inCartItems.filter(item => item.isChecked !== false);
			const fPrice = checkedPrice.map(item => item.price * item.quantity);

			var fP = 0;
			for (let i = 0; i < fPrice.length; i++) {
				fP += fPrice[i];
			}

			this.setState({
				finalPrice: fP
			});
		}
	};

	componentDidUpdate(prevProps, prevState) {
		const prices = this.state.inCartItems
			.filter(item => item.isChecked === true)
			.map(item => item.price * item.quantity);
		console.log(prices)
		let fPrice = 0;
		for (let i = 0; i < prices.length; i++) {
			fPrice += prices[i];
		}

		if (prevState.finalPrice !== fPrice) {
			this.setState({
				finalPrice: fPrice
			});

			const checkedItemsList = this.state.inCartItems.filter(item => item.isChecked === true);

			this.setState({
				checkedItems: checkedItemsList.length
			});

			if (this.state.inCartItems.length !== 0) {
				if (checkedItemsList.length === 0) {
					document.querySelector('input').checked = false;
					this.setState({
						allChecked: false
					});
				}
				if (checkedItemsList.length === this.state.inCartItems.length) {
					document.querySelector('input').checked = true;
					this.setState({
						allChecked: true
					});
				}
			}
		}
	}

	render() {
		const { state, action } = this;
		const stateAndAction = { state, action };

		return <Provider value={stateAndAction}>{this.props.children}</Provider>;
	}
}

export { StoreProvider, StoreConsumer };
