import React, {Component, createContext} from 'react'

const {Provider, Consumer:StoreConsumer} = createContext()
//20181028_wip
class StoreProvider extends Component{

    state = {
        inCartItems: [],
        inListItems: [
            {name:'싸구려', price:50, qty:1, isChecked:true},
            {name:'중저가', price:100, qty:1, isChecked:true},
            {name:'고오급', price:999, qty:1, isChecked:true},
        ],
        finalPrice: 0,
        allChecked: false
    }

    action = {
        inCart:(itemsToCart) => {
            if(this.state.inCartItems.filter((el) => el.name === itemsToCart.name).length > 0){
                const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === itemsToCart.name)
                this.setState({
                    //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                    qty: itemToIncrease[0].qty +=1,
                })
            }else{
                this.setState({
                    inCartItems: [...this.state.inCartItems, itemsToCart]
                    //비중복 넣을때는 초기값 1로 해주고 싶은데..전에 뺀적 있으면 그걸로 지정되어 버림
                })
            }
        },

        onIncrease:(currentItem) => {
            console.log(currentItem)
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                //아직도 이해가 안감 이 qty 가 내가 고른 아이템의 qty 라는 지정이 어디 있지?
                qty: itemToIncrease[0].qty +=1,
            })
        },

        onDecrease:(currentItem)=>{
            console.log(currentItem)
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                qty:itemToIncrease[0].qty -=1  
            })
   
            if(currentItem.qty < 1){
                this.setState({
                    inCartItems: this.state.inCartItems.filter( (item) => item.name !== currentItem.name  ),
                    qty:itemToIncrease[0].qty +=1
                })
            }
        },

        outCart:(itemsFromCart) => {
            console.log(itemsFromCart)
            this.setState({
                inCartItems: this.state.inCartItems.filter( (item) => item.name !== itemsFromCart.name  ),
                allChecked:false
            })
            
            //뺐다가 다시 애드하면 수량이 그대로 유지됨...1로 초기화 해야 하지 않을까?
        },


        //isChecked 는 state, chcked 는 dom 상의 클릭 여부... 같이 가야 하는데
        handleCheck:(selectedItem) => {
            selectedItem.isChecked = !selectedItem.isChecked //클릭시 상태 반전..
            // console.log('checkone:',selectedItem)
            const itemsToCheck = this.state.inCartItems.filter( (item)=> item.name === selectedItem)
            this.setState({isChecked:itemsToCheck.isChecked})
        },
        allCheckControl: () => {
            var allChk = document.getElementsByName('allCheck')
            var oneChks = document.getElementsByName('oneCheck')
            // console.log(oneChks)
            if(allChk[0].checked === true){ //전체선택이 켜지면
                oneChks.forEach((item)=>{(item.checked = true)}) //개별선택을 모두 켜고
                this.state.inCartItems.forEach(item => item.isChecked = true) //상태도 켜짐으로

                //카트내 모든 아이템의 가격을 합산해서 재반영
                // const fPrice = this.state.inCartItems.map( item => item.price )
                // var fP = 0;
                // for(let i =0; i<fPrice.length; i++){
                //     fP += fPrice[i]
                // }

                // this.setState({
                //     finalPrice: fP
                // })    

            }else{ //전체선택이 꺼지면
                // allChk[0].checked = false;
                oneChks.forEach((item)=>{(item.checked = false)}) //개별을 모두 꺼야..
                //state 도 false 로 해줘야 해..이걸 빼먹어서 안되었던 듯...!
                this.state.inCartItems.forEach(item => item.isChecked = false)
                // 그러면 억지로 0으로 안해줘도 자동으로 바뀌지 않을까?
                // this.setState({ 
                //     finalPrice:0
                // })
            }
            //꺼지든 켜지든 전체 금액 재계산은 동일하게 이뤄져야 할 것이다 조건문 밖에서...
            //된거 같은데!?!?!?!?!?!?!? 
            const fPrice = this.state.inCartItems.map( item => item.price * item.qty)
            var fP = 0;
            for(let i =0; i<fPrice.length; i++){
                fP += fPrice[i]
            }

            this.setState({
                finalPrice: fP
            })    


        }

    }


    componentDidUpdate(prevProps,prevState){
        const prices = this.state.inCartItems.filter(  (item) => item.isChecked === true ).map( (item) => item.price * item.qty  )

        var fPrice = 0; //let 쓰면 스코프가 안들어가는데 걍 var 말고는 방법 없나 let 쓰고싶은데..
        for(let i = 0; i<prices.length; i++){
            fPrice += prices[i];
        }

        //가격 변동이 있으면 적용.. 클릭여부랑 어디서 연동되지? 바로 위에
        if(prevState.finalPrice !== fPrice){
            this.setState({
                finalPrice:fPrice               
            })
        }
    }

    
    render(){

        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            <Provider value={stateAndAction}>
                {this.props.children}
            </Provider>
        )
    }

}

export {StoreProvider, StoreConsumer}