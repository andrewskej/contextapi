import React, {Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'
class CartList extends Component {

    render(){
        
        var cartStyle = {
            'position':'absolute',
            'top':'2%',            
            'border':'1px solid darkgray',
            'width':'80%',
            'height':'45%',
            'overflowY':'scroll',
            'margin':'1%',
            'left':'50%',
            'transform':'translateX(-48%)',
            'padding':'3%',
            'textAlign':'center'
        }

        var cartItemStyle = {
            'borderTop':'1px solid darkgray',
            'borderBottom':'1px solid darkgray',
            'width':'98%',
            'height':'20%',
            'marginTop':'15%',
            'marginLeft':'50%',
            'marginBottom':'-15%',
            'transform':'translateX(-50%)',
            'padding':'5%',
            'fontWeight':'bolder'
        }

        var buttonStyle1 = {
            'border':'1px solid gray',
            'background':'white',
            'marginTop':'6%',
            'color':'black',
            'fontSize':'18px',
        }

        var allCheckBoxStyle = {
            'position':'fixed',
            'border':'1px solid black',
            'width':'25px',
            'height':'25px',
            'left':'3%'            
        }

        var checkBoxStyle = {
            'position':'fixed',
            'width':'25px',
            'height':'25px',
            'left':'3%',
            'top':'40%' 
        }

        var removeBoxStyle = {
            'position':'absolute',
            'border':'2px solid darkgray',
            'background':'white',
            'color':'gray',
            'width':'25px',
            'height':'25px',
            'fontSize':'18px',
            'right':'3%',
            'fontWeight':'bolder',
            'paddingRight':'5%'            
        }

        var titleStyle = {
            'position':'absolute',
            'height':'10%',
            'left':'15%',
            'top':'6%'
        }

        var imgStyle = {
            'width':'25%',
            'height': '70%',
            'position': 'absolute',
            'left': '15%',
            'top': '18%',
            'marginBottom':'2%',
            'border': '1px solid gray'
        }

        var itemTextStyle = {
            'position':'absolute',
            'left':'52%'
        }

    return(
            <StoreConsumer>
                {( { state, action } ) => (
                    (state.inCartItems.length>0)
                    ?
                    <div style={cartStyle}> 
                            <div>
                                <input type="checkBox" name="allCheck" defaultChecked style={allCheckBoxStyle}  onClick={() => action.allCheckControl()}/> 
                                <span style={titleStyle}> 전체선택 ({state.checkedItems} / {state.inCartItems.length})  </span>
                            </div>

                            {
                                Object.entries(state.inCartItems).map((item, i) => {
                                    return <div style={cartItemStyle} key={i} name={item[1].name}>
                                                <input style={checkBoxStyle} name="oneCheck" type="checkBox" defaultChecked onChange={()=> action.handleCheck(item[1])}/>
                                                <button style={removeBoxStyle} onClick={ () => action.outCart(item[1])}>X</button>
                                                <img style={imgStyle} src={ `${item[1].image.href}` } alt="item_img"></img>
                                                <div style={itemTextStyle}>

                                                    <div>
                                                        <div>{`${item[1].name}`}</div>
                                                        <div>{`$: ${item[1].price}`}</div>
                                                    </div>

                                                    <div>
                                                        <button style={buttonStyle1} onClick={ () => action.onDecrease(item[1]) }>-</button>
                                                        <button style={buttonStyle1}>{item[1].quantity}</button>
                                                        <button style={buttonStyle1} onClick={ () => action.onIncrease(item[1]) }>+</button>
                                                    </div>

                                                </div>

                                            </div>

                                    })
                            }

                    </div>
                    :
                    <div style={cartStyle}>카트가 비었습니다</div>

                    )}
            </StoreConsumer>
        )
    }



}

export default CartList