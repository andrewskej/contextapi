import React ,{Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'
// import Item from './Item' 일단 얘 빼

//여기의 정보를 CartList 로 보내려고 한다

class ItemList extends Component {
 
    state = {
        items:[
            {name:'test1', price:500},
            {name:'test2', price:600},
            {name:'test3', price:700},
        ]
    }

    // state = {
    //     item1:'simpleTest',
    //     item2:'item2',
    //     item3:'item3'
    // }

    //여기 state 는 인풋 받은 값을 
    //store에 보내고 다시 내려받아서 업데이트 하려고 비워놓은거?맞나?
    //다시 내려받을 필요 없으면 신경 안써도 되나?
    //여길 채우면 아이템리스트의 초기값이 되겠지? 안되네? 왜냐면 
    //밑에 didmount 에서 state reset 시켜버리기 때문이지

    //ItemList가 생겨나서 마운트 되면:
    componentDidMount(){
        //ItemList 의 state를 세팅해줘
        this.setState({
        //뭘로? ItemList의 prop 의 value
        //props는 어디? store 에서 가져오네?
        //즉 store 에 있는 값을 끌어온다?
        // input:this.props.value

        //여기서 기본값 담아두고 보내려면 어떻게 바꿀까? 요렇게
        //    input:this.state.item2  이거 필요 없는듯..?
      })
    }
    

    //변화가 있으면(이벤트 받아서) (input 뺐으니 필요 없지?)
    // handleChange = (e) => {
    //     //ItemList의 state를 바꿔
    //     //input 이라는 state의 변수명을, 입력받은 이벤트의 값으로 바꿔
    //     this.setState({input:e.target.value})
    // }

    //submit 누르면
    // handleSubmit = (e) => {
        // e.preventDefault();
        /*얘의 prop(Store) 에 있는 setvalue 이용해서 
        이 페이지에서 input 한 state 값을 반영 */
        //얘를 누적되게 바꾸면 값이 쌓이려나? 아니고 Store였다
        // console.log(this.state)
        // this.props.setValue(this.state.item2)
        //이제 input이 아닌 고정값을 보내는 걸로 바꾸려면?
        //여기서 말한 input 은 state 내의 변수명이었어...ㅠㅠ
        // this.props.setValue(e.target)//되겠냐 이게..

        // console.log(e.target)
        //왜 value는 안나오지?
        // console.log(e.target.value)
    // }

    //submit 없애고 click으로 바꿔도..
    handleClick = (e) => {
        console.log(e.target)
        console.log(this.state)
        //왜 e.target.value는 안나와...?
        //어떻게 해야 클릭한 값을 뽑아서 보내주지?
        this.props.setValue(this.state.item2)
    }

    // input으로 직접 입력해서 보낼때
    // render(){
    //     return(
    //         <form onSubmit={this.handleSubmit}>
    //             아이템: <input value={this.state.input} onChange={this.handleChange}/>
    //             <button type="submit">쏴</button>
    //         </form>
    //     )
    // }
    
    // 어떻게 고치지? 하나만 보낼땐 이렇게 보내진다
    // render(){
    //     return(
    //         <form onSubmit={this.handleSubmit}>
    //             아이템: <div>{this.state.input}</div>
    //             <button type="submit">쏴</button>
    //         </form>
    //     )
    // }

    // render(){  //여러개를 각각의 버튼을 달아서...근데 뭘 쏴도 맨 위가 간다
    //     return(
    //         <div>
    //             아이템: 
    //             <form value={this.state.input} onSubmit={this.handleSubmit}>
    //                     {this.state.input}
    //             <button type="submit">쏴</button>
    //             </form>

    //             <form value={this.state.item2} onSubmit={this.handleSubmit}>
    //                     {this.state.item2}
    //             <button type="submit">쏴</button>
    //             </form>

    //             <form value={this.state.item3} onSubmit={this.handleSubmit}>
    //                     {this.state.item3}
    //             <button type="submit">쏴</button>
    //             </form>
    //         </div>  
    //     )
    // }

    render(){  //form 일 필요 없지 않을까? 안되네 그럼 온클릭으로 바꾸면?
      //이런식으로 map 하는게 아니던가?
        const list = this.state.items.map( (item,i) => <div key={i} name={item.name}  price={item.price} />)
        return(
            <div>
                아이템: 
                <div value={this.state.item1} onClick={this.handleClick}>
                    {this.state.item1}
                    {list}
                </div>
                <div value={this.state.item2} onClick={this.handleClick}>
                    {this.state.item2}
                </div>
                <div value={this.state.item3} onClick={this.handleClick}>
                    {this.state.item3}
                </div>
            </div>  
         
        )
    }

}

//여긴 뭐야 여긴 왜 만들었어?
const ItemListContainer = () => (
    
    //Consumer 인스턴스로 감싸서
    //얘가 필요한 정보나 메소드를 Store에서 꺼내올수 있게 권한을 준다?
    <StoreConsumer>
        {
            ({state, action}) => (
                // ItemList에서 ItemList 를 만들었네?
                // 위에서 render/return 에서 만든 dom 을 가져다가 쓰고,
                // state(props인 store에서 끌어온 값으로 여기 ItemList의 state 값을 정해줬지) 
                // 그리고 action(store에서 가져온 메소드)을 부여해 주는거야. 왜? 어떻게?
                <ItemList
                    value={state.value}
                    setValue={action.setValue}
                />
            )
        }
    </StoreConsumer>
)



// 왜 저런걸 내보내지?
/*
이번엔 우리가 기존에 준비했던 
action 를 통해서 값을 변경해보겠습니다. 
이번에 컴포넌트를 구현하기 위해선, 
단순히 render 에서만 필요한게 아니라, 
내부에 있는 메소드에서도 필요로 합니다. 
그렇기에 아까전에 했던것처럼 
render 에서 Consumer 를 사용하는 형태로 
구현하지 않고, SendsContainer 라는 
컨테이너 컴포넌트를 추가적으로 만들어서 
props 로 필요한 값을 전달하는 방식으로 
구현해주겠습니다.*/
// export default ItemList
export default ItemListContainer