import React, {Component} from 'react'
import {StoreConsumer} from '../Contexts/Store'
import Coupons from './Coupons'
class CheckOut extends Component{

    state = {
        couponSale: ''
    }

    handleChange = () => {
        var selected = document.getElementsByClassName('coupons')[0];
        var saleP = selected[selected.selectedIndex].attributes.sale

        this.setState({
            couponSale: saleP.textContent || 0
        })

    }



    render(){

        var checkOutStyle={
            'position':'absolute',
            'bottom':'21%',
            'left':'50%',
            'transform':'translateX(-48%)',
            'border':'solid 2px black',
            'width':'60%',
            'height':'20%',
            'padding':'3%',
            'margin':'3%',
            'marginTop':'2%',
            'marginBottom':'5%',
            'textAlign':'center',
            'fontWeight':'bold'
        }

        var orderStyle={
            'position':'absolute',
            'left':'10%'
        }

        var couponStyle ={
            'position':'absolute',
            'border':'1px solid gray',
            'height':'15%',
            'width':'75%',
            'left':'10%',
            'bottom':'64%'
        }

        var saleStyle = {
            'position':'absolute',
            'left':'10%',
            'bottom':'45%'
        }

        var finalStyle={
            'color':'orangeRed',
            'position':'absolute',
            'bottom':'28%',
            'left':'10%'
        }

        var bonusStyle={
            'position':'absolute',
            'bottom':'10%',
            'left':'10%',
        }

        return(
            <StoreConsumer>
                
                {(store) => (

                    <div style={checkOutStyle}>
                        <div style={orderStyle}>주문 금액 {store.state.finalPrice}</div>

                        <select className="coupons" style={couponStyle} onChange={this.handleChange}>
                            <option value="selected" sale="0">쿠폰 선택</option>
                            <Coupons/>
                        </select>
                        
                        <div style={saleStyle}> 쿠폰할인액: {this.state.couponSale} </div>

                        <div className="finalPrice">
                        {
                            (this.state.couponSale % 1 === 0 )
                            ? <span style={finalStyle}>최종 금액: $ {store.state.finalPrice - this.state.couponSale}</span>
                            : <span style={finalStyle}>최종 금액: $ {store.state.finalPrice * (1 -  ( Number(this.state.couponSale.split('%')[0]) /100 )) }</span>
                        }
                        </div>

                        <div style={bonusStyle}> 적립금(15%): $ {store.state.finalPrice * 0.15} </div>

                    </div>
                
                )}   
            </StoreConsumer>
        )

    }


}

export default CheckOut