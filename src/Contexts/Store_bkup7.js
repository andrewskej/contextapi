import React, {Component, createContext} from 'react'


const {Provider, Consumer:StoreConsumer} = createContext()
class StoreProvider extends Component{

    state = {
        inCartItems: [],
        inListItems: [
            {name:'싼거', price:50, qty:1, isChecked:true},
            {name:'최저가', price:80, qty:1, isChecked:true},
            {name:'중저가', price:100, qty:1, isChecked:true},
            {name:'고오급', price:1000, qty:1, isChecked:true},
            {name:'최고급', price:9000, qty:1, isChecked:true},
            {name:'최고가', price:75000, qty:1, isChecked:true},
            {name:'진짜비싼거', price:92560, qty:1, isChecked:true},
            {name:'쓸데없는거', price:25000, qty:1, isChecked:true},
            {name:'갖고싶은거', price:350000, qty:1, isChecked:true},
            {name:'별거아닌거', price:5000, qty:1, isChecked:true},
        ],
        finalPrice: 0,
        allChecked: true,
        checkedItems: 0   
    }

    action = {
        inCart:(itemsToCart) => {
            if(this.state.inCartItems.filter((el) => el.name === itemsToCart.name).length > 0){
                const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === itemsToCart.name)
                this.setState({
                    qty: itemToIncrease[0].qty +=1,
                })
            }else{
                this.setState({
                    inCartItems: [...this.state.inCartItems, itemsToCart]
                })

            }
        },

        onIncrease:(currentItem) => {
            const itemToIncrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                qty: itemToIncrease[0].qty +=1,
            })
        },

        onDecrease:(currentItem)=>{
            const itemToDecrease = this.state.inCartItems.filter((el)=>el.name === currentItem.name)
            this.setState({
                qty:itemToDecrease[0].qty -=1  
            })

            //여기 버그가 있는듯 하다...언체크 되어있는 아이템을 - 나 x버튼으로 빼면 다음 index 체크드아이템에 체크가 풀려...
            //가격은 유지된다...
            if(currentItem.qty < 1){
                this.setState({
                    inCartItems: this.state.inCartItems.filter( (item) => item.name !== currentItem.name),
                    qty:itemToDecrease[0].qty +=1
                })
            }
        },


        //여기 버그가 있는듯 하다...언체크 되어있는 아이템을 - 나 x버튼으로 빼면 다음 index 체크드아이템에 체크가 풀려...
        //가격은 유지된다...
        outCart:(itemsFromCart) => {
            itemsFromCart.qty = 1   
            this.setState({
                inCartItems: this.state.inCartItems.filter( (item) => item.name !== itemsFromCart.name),
                allChecked:false
            })
        },

        //복수 선택 삭제하기 신규 구현 필요 

        handleCheck:(selectedItem) => {
            selectedItem.isChecked = !selectedItem.isChecked 
            const itemsToCheck = this.state.inCartItems.filter( (item)=> item.name === selectedItem)
            this.setState({
                isChecked:itemsToCheck.isChecked,
            })
        },
        allCheckControl: () => {
            var allChk = document.querySelector('input')
            var oneChks = document.getElementsByName('oneCheck')

            if(allChk.checked === true){ 
                oneChks.forEach((item)=>{(item.checked = true)}) 
                this.state.inCartItems.forEach(item => item.isChecked = true) 

            }else{ 
                oneChks.forEach((item)=>{(item.checked = false)}) 
                this.state.inCartItems.forEach(item => item.isChecked = false)
            }

            const checkedPrice = this.state.inCartItems.filter(item => item.isChecked !== false)
            const fPrice = checkedPrice.map(item => item.price * item.qty)

           
            var fP = 0;
            for(let i =0; i<fPrice.length; i++){
                fP += fPrice[i]
            }

            this.setState({
                finalPrice: fP
            })    

        }

    }

    //여기서 덜어내서 각 메소드로 보낼수 있을 코드는 발라내서 분배 해보자 여기때문에 로직이 꼬이는것 같다
    componentDidUpdate(prevProps,prevState){
        const prices = this.state.inCartItems.filter(  (item) => item.isChecked === true ).map( (item) => item.price * item.qty  )

        let fPrice = 0; 
        for(let i = 0; i<prices.length; i++){
            fPrice += prices[i];
        }

        if(prevState.finalPrice !== fPrice){
            this.setState({
                finalPrice:fPrice               
            })
            
            const checkedItemsNo = this.state.inCartItems.filter( item => item.isChecked === true)
           
            this.setState({
                checkedItems: checkedItemsNo.length
            })

            if(this.state.inCartItems.length!==0){

                if(checkedItemsNo.length === 0){
                    document.querySelector('input').checked = false
                    this.setState({
                        allChecked:false
                    })
                }
                if(checkedItemsNo.length === this.state.inCartItems.length){
                    document.querySelector('input').checked = true
                    this.setState({
                        allChecked: true
                    })
                }

            }

        }

        //state 는 유지되고 실제 체크만 빠지는 거니까 state 에 맞게 실제 체크를 먹여 (actual체크 = state체크)

        // console.log(this.state.inCartItems.forEach((el,i) => console.log('state 체크여부:',i, el.isChecked)))
        // const stateCheck = this.state.inCartItems.forEach(el => el.isChecked)

        var stateCheck = [];
        for(let i=0; i<=this.state.inCartItems.length-1;i++){ //-1 을 넣는건 너무 야매 아니여?
            stateCheck.push(this.state.inCartItems[i].isChecked) //왜 마지막에 undefined 가 하나 낑겨 있어???아이템이 빠진 상태?
        }
        console.log('stateCheck:',stateCheck) 
 
        const eachChk = document.getElementsByName('oneCheck')
        var actualCheck = [];
        for(let i = 0; i<eachChk.length; i++){
            actualCheck.push(eachChk[i].checked)
        }
        console.log('actualCheck:',actualCheck) 

        //actualCheck = stateCheck 로 만들어야돼
        // for(var i=0; i<actualCheck.length; i++){
        //     actualCheck[i] = stateCheck[i]
        // }

    }

    
    render(){

        const {state, action} = this;
        const stateAndAction = {state,action};

        return(
            <Provider value={stateAndAction}>
                {this.props.children}
            </Provider>
        )
    }

}

export {StoreProvider, StoreConsumer}