import React from 'react'
import {StoreConsumer} from '../Contexts/Store'

//ItemList 에서 보낸걸 여기서 받으려고 한다
//얘는 클래스가 아니고 그냥 함수. 차이는?
//클래스일땐 뭐가 가능하고 아닐땐 뭐가 불가능하지?
const CartList = () => {

//얘는 state가 필요 없네? 왜지?
//받아서 보여주기만 해서?

return(
    //Consumer로 랩했다는 것은 Provider(Store.js)의 데이터를 
    //가져다가 쓰겠다는 뜻이다. 맞지? action도 접근 가능
    <StoreConsumer>
         {( { state, action } ) => (
             <div>
             CART motherFucker
                    {
                    //     // 왜 item[1]로 해야 나오지?? 왜 배열을 두바퀴 돌지?
                    Object.entries(state.inCartItems).map((item, i) => {
                        return <div key={i} name={item[1].name}>{`${item[1].name} | ${item[1].price}`}
                        <button onClick={  () => action.outCart(item[1])}>remove</button></div>;
                    })

                }
                </div>
            )}
    </StoreConsumer>
)



}

export default CartList